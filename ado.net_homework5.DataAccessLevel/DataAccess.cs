﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ado.net_homework5.DataAccessLevel
{
    public class DataAccess
    {

        public void CustomersTableInit(ref DataTable customers)
        {
            DataColumn id = new DataColumn("id", typeof(int));
            id.AllowDBNull = false;
            id.AutoIncrement = true;
            id.AutoIncrementSeed = 1;
            id.AutoIncrementStep = 1;
            id.Unique = true;
            id.ReadOnly = true;

            DataColumn fio = new DataColumn("fio", typeof(string));
            fio.AllowDBNull = false;
            fio.MaxLength = 60;

            DataColumn address = new DataColumn("address", typeof(string));
            fio.AllowDBNull = false;
            fio.MaxLength = 60;

            customers.Columns.AddRange(new DataColumn[]
            {
                id,
                fio,
                address
            });
            customers.PrimaryKey = new DataColumn[] { customers.Columns["id"] };
        }

        public void ProductsTableInit(ref DataTable products)
        {
            DataColumn id = new DataColumn("id", typeof(int));
            id.AllowDBNull = false;
            id.AutoIncrement = true;
            id.AutoIncrementSeed = 1;
            id.AutoIncrementStep = 1;
            id.Unique = true;
            id.ReadOnly = true;

            DataColumn productName = new DataColumn("productName", typeof(string))
            {
                AllowDBNull = false,

            };
            DataColumn productPrice = new DataColumn("productPrice", typeof(string))
            {
                AllowDBNull = false,
            };

            products.Columns.AddRange(new DataColumn[]
            {
                id,
                productName,
                productPrice
            });
            products.PrimaryKey = new DataColumn[] { products.Columns["id"] };
        }

        public void EmployeesTableInit(ref DataTable employees)
        {
            DataColumn id = new DataColumn("id", typeof(int));
            id.AllowDBNull = false;
            id.AutoIncrement = true;
            id.AutoIncrementSeed = 1;
            id.AutoIncrementStep = 1;
            id.Unique = true;
            id.ReadOnly = true;

            DataColumn fio = new DataColumn("fio", typeof(string));
            fio.AllowDBNull = false;
            fio.MaxLength = 60;

            DataColumn position = new DataColumn("position", typeof(string));
            fio.AllowDBNull = false;
            fio.MaxLength = 60;

            employees.Columns.AddRange(new DataColumn[]
           {
               id,
               fio,
               position
           });
            employees.PrimaryKey = new DataColumn[] { employees.Columns["id"] };
        }

        public void OrderDetailsTableInit(ref DataTable orderDetails)
        {
            DataColumn id = new DataColumn("id", typeof(int));
            id.AllowDBNull = false;
            id.AutoIncrement = true;
            id.AutoIncrementSeed = 1;
            id.AutoIncrementStep = 1;
            id.Unique = true;
            id.ReadOnly = true;

            DataColumn details = new DataColumn("details", typeof(string));
            details.AllowDBNull = true;
            details.MaxLength = 200;

            orderDetails.Columns.AddRange(new DataColumn[]
         {
               id,
               details
         });
            orderDetails.PrimaryKey = new DataColumn[] { orderDetails.Columns["id"] };
        }

        public void OrdersTableInit(ref DataTable orders)
        {
            DataColumn id = new DataColumn("id", typeof(int));
            id.AllowDBNull = false;
            id.AutoIncrement = true;
            id.AutoIncrementSeed = 1;
            id.AutoIncrementStep = 1;
            id.Unique = true;
            id.ReadOnly = true;

            DataColumn customerId = new DataColumn("customerId", typeof(int));
            customerId.AllowDBNull = false;

            DataColumn productId = new DataColumn("productId", typeof(int));
            customerId.AllowDBNull = false;

            DataColumn employeeId = new DataColumn("employeeId", typeof(int));
            customerId.AllowDBNull = false;

            DataColumn orderDetailsId = new DataColumn("orderDetailsId", typeof(int));
            customerId.AllowDBNull = true;

            orders.Columns.AddRange(new DataColumn[]
         {
                id,
                customerId,
                productId,
                employeeId,
                orderDetailsId
         });
            orders.PrimaryKey = new DataColumn[] { orders.Columns["id"] };
        }
    }
}
