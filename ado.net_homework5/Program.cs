﻿using ado.net_homework5.DataAccessLevel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ado.net_homework5
{
    class Program
    {
        static void Main(string[] args)
        {
            DataSet ShopDB = new DataSet("ShopDB");

            DataTable orders = new DataTable("Orders");
            DataTable customers = new DataTable("Customers");
            DataTable employees = new DataTable("Employees");
            DataTable orderDetails = new DataTable("OrderDetails");
            DataTable products = new DataTable("Products");

            DataAccess dataAccess = new DataAccess();
            dataAccess.OrdersTableInit(ref orders);
            dataAccess.CustomersTableInit(ref customers);
            dataAccess.EmployeesTableInit(ref employees);
            dataAccess.OrderDetailsTableInit(ref orderDetails);
            dataAccess.ProductsTableInit(ref products);


            ForeignKeyConstraint FK_customers = new ForeignKeyConstraint(customers.Columns["id"], orders.Columns["customerId"]);
            orders.Constraints.Add(FK_customers);
            ForeignKeyConstraint FK_products = new ForeignKeyConstraint(products.Columns["id"], orders.Columns["productId"]);
            orders.Constraints.Add(FK_products);
            ForeignKeyConstraint FK_employees = new ForeignKeyConstraint(employees.Columns["id"], orders.Columns["employeeId"]);
            orders.Constraints.Add(FK_employees);
            ForeignKeyConstraint FK_orderDetails = new ForeignKeyConstraint(orderDetails.Columns["id"], orders.Columns["orderDetailsId"]);
            orders.Constraints.Add(FK_orderDetails);

            ShopDB.Tables.AddRange(new DataTable[]
                {
                    orders,
                    customers,
                    employees,
                    orderDetails,
                    products
                });

            orders.WriteXml("orders.xml");

        }
    }
}
